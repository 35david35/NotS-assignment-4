﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HttpServers
{
    class HttpServer
    {
        private readonly int _port;
        private TcpListener _listener;
        private readonly bool _isActive = true;

        public HttpServer(int port)
        {
            this._port = port;
        }

        public void Listen()
        {
            _listener = new TcpListener(IPAddress.Any, _port);
            _listener.Start();
            while (_isActive)
            {
                var s = _listener.AcceptTcpClient();
                var processor = new HttpProcessor(s, this);
                var thread = new Thread(new ThreadStart(processor.process));
                thread.Start();
                Thread.Sleep(1);
            }
        }

        public void HandleGetRequest(HttpProcessor p)
        {
            var requestFile = p.http_url == "/" ? "index.html" : p.http_url;
            var file = System.IO.Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\TestWebsite\\" + requestFile;

            if (!File.Exists(file))
            {
                p.writeFailure();
            }

            var bytes = File.ReadAllBytes(file);
            var contentType = (string)p.httpHeaders["Content-Type"];
            p.writeSuccess(contentType);
            p.outputStream.BaseStream.Write(bytes, 0, bytes.Length);
        }

        public void HandlePostRequest(HttpProcessor p, StreamReader inputData)
        {
            Console.WriteLine("POST request: {0}", p.http_url);
            string data = inputData.ReadToEnd();

            p.writeSuccess();
//            p.outputStream.WriteLine("<html><body><h1>test server</h1>");
//            p.outputStream.WriteLine("<a href=/test>return</a><p>");
//            p.outputStream.WriteLine("postbody: <pre>{0}</pre>", data);
        }
    }
}
