﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HttpServers
{
    class Program
    {
        static void Main(string[] args)
        {
            var httpServer = args.GetLength(0) > 0 ? new HttpServer(Convert.ToInt16(args[0])) : new HttpServer(8080);
            var thread = new Thread(new ThreadStart(httpServer.Listen));
            thread.Start();
        }
    }
}
