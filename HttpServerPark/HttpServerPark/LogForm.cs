﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class LogForm : Form
    {

        private string _serverInfo;

        /// <summary>
        /// LogForm Contructor
        /// </summary>
        /// <param name="info">Server info</param>
        public LogForm(string info)
        {
            InitializeComponent();
            _serverInfo = info;
            lblServer.Text = _serverInfo;
        }

        /// <summary>
        /// Method for clearing listbox when clicking Clear button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearLog_Click(object sender, EventArgs e)
        {
            lstCentralLog.Items.Clear();
        }
    }
}
