﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class ServerForm : Form
    {
        private int _serverPort;
        private string _serverName;
        private string _serverTxt;

        /// <summary>
        /// Serverform Constructor
        /// </summary>
        /// <param name="serverName">Name of the server</param>
        /// <param name="serverPort">Port of the server</param>
        public ServerForm(string serverName, int serverPort)
        {
            InitializeComponent();
            _serverPort = serverPort;
            _serverName = serverName;
            lblServerPortValue.Text = serverPort.ToString();
        }

        /// <summary>
        /// Show log of specific server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowLog_Click(object sender, EventArgs e)
        {
            _serverTxt = _serverName + ":" + _serverPort;

            LogForm serverLog = new LogForm(_serverTxt);
            serverLog.Show();
        }

        /// <summary>
        /// Method to enter just numbers 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numbersOnly_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            
        }
    }
}
