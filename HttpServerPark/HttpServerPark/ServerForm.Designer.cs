﻿namespace WindowsFormsApplication1
{
    partial class ServerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblMemoryUsageValue = new System.Windows.Forms.Label();
            this.lblMemoryUsage = new System.Windows.Forms.Label();
            this.lblActiveClientsValue = new System.Windows.Forms.Label();
            this.lblActiveClients = new System.Windows.Forms.Label();
            this.lblCachedFilesValue = new System.Windows.Forms.Label();
            this.lblCachedFiles = new System.Windows.Forms.Label();
            this.lblActiveThreadsValue = new System.Windows.Forms.Label();
            this.lblActiveThreads = new System.Windows.Forms.Label();
            this.lblServerPortValue = new System.Windows.Forms.Label();
            this.lblServerPort = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnShowLog = new System.Windows.Forms.Button();
            this.caching = new System.Windows.Forms.GroupBox();
            this.txtCacheTimeout = new System.Windows.Forms.TextBox();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.lblInterval = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1.SuspendLayout();
            this.caching.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblMemoryUsageValue);
            this.groupBox1.Controls.Add(this.lblMemoryUsage);
            this.groupBox1.Controls.Add(this.lblActiveClientsValue);
            this.groupBox1.Controls.Add(this.lblActiveClients);
            this.groupBox1.Controls.Add(this.lblCachedFilesValue);
            this.groupBox1.Controls.Add(this.lblCachedFiles);
            this.groupBox1.Controls.Add(this.lblActiveThreadsValue);
            this.groupBox1.Controls.Add(this.lblActiveThreads);
            this.groupBox1.Controls.Add(this.lblServerPortValue);
            this.groupBox1.Controls.Add(this.lblServerPort);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(239, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selected Server";
            // 
            // lblMemoryUsageValue
            // 
            this.lblMemoryUsageValue.AutoSize = true;
            this.lblMemoryUsageValue.Location = new System.Drawing.Point(161, 71);
            this.lblMemoryUsageValue.Name = "lblMemoryUsageValue";
            this.lblMemoryUsageValue.Size = new System.Drawing.Size(21, 13);
            this.lblMemoryUsageValue.TabIndex = 9;
            this.lblMemoryUsageValue.Text = "0%";
            // 
            // lblMemoryUsage
            // 
            this.lblMemoryUsage.AutoSize = true;
            this.lblMemoryUsage.Location = new System.Drawing.Point(6, 71);
            this.lblMemoryUsage.Name = "lblMemoryUsage";
            this.lblMemoryUsage.Size = new System.Drawing.Size(81, 13);
            this.lblMemoryUsage.TabIndex = 8;
            this.lblMemoryUsage.Text = "Memory Usage:";
            // 
            // lblActiveClientsValue
            // 
            this.lblActiveClientsValue.AutoSize = true;
            this.lblActiveClientsValue.Location = new System.Drawing.Point(161, 58);
            this.lblActiveClientsValue.Name = "lblActiveClientsValue";
            this.lblActiveClientsValue.Size = new System.Drawing.Size(13, 13);
            this.lblActiveClientsValue.TabIndex = 7;
            this.lblActiveClientsValue.Text = "0";
            // 
            // lblActiveClients
            // 
            this.lblActiveClients.AutoSize = true;
            this.lblActiveClients.Location = new System.Drawing.Point(6, 58);
            this.lblActiveClients.Name = "lblActiveClients";
            this.lblActiveClients.Size = new System.Drawing.Size(74, 13);
            this.lblActiveClients.TabIndex = 6;
            this.lblActiveClients.Text = "Active Clients:";
            // 
            // lblCachedFilesValue
            // 
            this.lblCachedFilesValue.AutoSize = true;
            this.lblCachedFilesValue.Location = new System.Drawing.Point(161, 45);
            this.lblCachedFilesValue.Name = "lblCachedFilesValue";
            this.lblCachedFilesValue.Size = new System.Drawing.Size(13, 13);
            this.lblCachedFilesValue.TabIndex = 5;
            this.lblCachedFilesValue.Text = "0";
            // 
            // lblCachedFiles
            // 
            this.lblCachedFiles.AutoSize = true;
            this.lblCachedFiles.Location = new System.Drawing.Point(6, 45);
            this.lblCachedFiles.Name = "lblCachedFiles";
            this.lblCachedFiles.Size = new System.Drawing.Size(71, 13);
            this.lblCachedFiles.TabIndex = 4;
            this.lblCachedFiles.Text = "Cached Files:";
            // 
            // lblActiveThreadsValue
            // 
            this.lblActiveThreadsValue.AutoSize = true;
            this.lblActiveThreadsValue.Location = new System.Drawing.Point(161, 30);
            this.lblActiveThreadsValue.Name = "lblActiveThreadsValue";
            this.lblActiveThreadsValue.Size = new System.Drawing.Size(13, 13);
            this.lblActiveThreadsValue.TabIndex = 3;
            this.lblActiveThreadsValue.Text = "0";
            // 
            // lblActiveThreads
            // 
            this.lblActiveThreads.AutoSize = true;
            this.lblActiveThreads.Location = new System.Drawing.Point(6, 30);
            this.lblActiveThreads.Name = "lblActiveThreads";
            this.lblActiveThreads.Size = new System.Drawing.Size(82, 13);
            this.lblActiveThreads.TabIndex = 2;
            this.lblActiveThreads.Text = "Active Threads:";
            // 
            // lblServerPortValue
            // 
            this.lblServerPortValue.AutoSize = true;
            this.lblServerPortValue.Location = new System.Drawing.Point(161, 16);
            this.lblServerPortValue.Name = "lblServerPortValue";
            this.lblServerPortValue.Size = new System.Drawing.Size(35, 13);
            this.lblServerPortValue.TabIndex = 1;
            this.lblServerPortValue.Text = "label1";
            // 
            // lblServerPort
            // 
            this.lblServerPort.AutoSize = true;
            this.lblServerPort.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lblServerPort.Location = new System.Drawing.Point(6, 16);
            this.lblServerPort.Name = "lblServerPort";
            this.lblServerPort.Size = new System.Drawing.Size(63, 13);
            this.lblServerPort.TabIndex = 0;
            this.lblServerPort.Text = "ServerPort: ";
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(3, 440);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(239, 25);
            this.button1.TabIndex = 1;
            this.button1.Text = "Stop Server";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnShowLog
            // 
            this.btnShowLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnShowLog.Location = new System.Drawing.Point(3, 380);
            this.btnShowLog.Name = "btnShowLog";
            this.btnShowLog.Size = new System.Drawing.Size(239, 24);
            this.btnShowLog.TabIndex = 2;
            this.btnShowLog.Text = "Show server log";
            this.btnShowLog.UseVisualStyleBackColor = true;
            this.btnShowLog.Click += new System.EventHandler(this.btnShowLog_Click);
            // 
            // caching
            // 
            this.caching.Controls.Add(this.txtCacheTimeout);
            this.caching.Controls.Add(this.numericUpDown2);
            this.caching.Controls.Add(this.label1);
            this.caching.Controls.Add(this.lblInterval);
            this.caching.Dock = System.Windows.Forms.DockStyle.Fill;
            this.caching.Location = new System.Drawing.Point(3, 103);
            this.caching.Name = "caching";
            this.caching.Size = new System.Drawing.Size(239, 109);
            this.caching.TabIndex = 3;
            this.caching.TabStop = false;
            this.caching.Text = "Caching";
            // 
            // txtCacheTimeout
            // 
            this.txtCacheTimeout.Location = new System.Drawing.Point(7, 37);
            this.txtCacheTimeout.Name = "txtCacheTimeout";
            this.txtCacheTimeout.Size = new System.Drawing.Size(225, 20);
            this.txtCacheTimeout.TabIndex = 4;
            this.txtCacheTimeout.Text = "0";
            this.txtCacheTimeout.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numbersOnly_KeyPress);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(6, 81);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(226, 20);
            this.numericUpDown2.TabIndex = 3;
            this.numericUpDown2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numbersOnly_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Buffersize in bytes:";
            // 
            // lblInterval
            // 
            this.lblInterval.AutoSize = true;
            this.lblInterval.Location = new System.Drawing.Point(6, 20);
            this.lblInterval.Name = "lblInterval";
            this.lblInterval.Size = new System.Drawing.Size(72, 13);
            this.lblInterval.TabIndex = 0;
            this.lblInterval.Text = "Interval in ms:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numericUpDown3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 218);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(239, 56);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Threads";
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.numericUpDown3.Location = new System.Drawing.Point(3, 33);
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(233, 20);
            this.numericUpDown3.TabIndex = 1;
            this.numericUpDown3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numbersOnly_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Max amount of threads:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numericUpDown4);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 280);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(239, 44);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Buffersize";
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.numericUpDown4.Location = new System.Drawing.Point(3, 21);
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(233, 20);
            this.numericUpDown4.TabIndex = 0;
            this.numericUpDown4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numbersOnly_KeyPress);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.comboBox1);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 330);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(239, 44);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Content-Encoding";
            // 
            // comboBox1
            // 
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Gzip",
            "Deflate",
            "Chunked(Transfer-Encoding)"});
            this.comboBox1.Location = new System.Drawing.Point(3, 20);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(233, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.Text = "Gzip";
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSaveSettings.Location = new System.Drawing.Point(3, 410);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(239, 24);
            this.btnSaveSettings.TabIndex = 7;
            this.btnSaveSettings.Text = "Save settings";
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.caching, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.groupBox4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnSaveSettings, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.btnShowLog, 0, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(245, 468);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // ServerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(245, 468);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(260, 507);
            this.Name = "ServerForm";
            this.ShowIcon = false;
            this.Text = "ServerForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.caching.ResumeLayout(false);
            this.caching.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblActiveThreadsValue;
        private System.Windows.Forms.Label lblActiveThreads;
        private System.Windows.Forms.Label lblServerPortValue;
        private System.Windows.Forms.Label lblServerPort;
        private System.Windows.Forms.Label lblCachedFilesValue;
        private System.Windows.Forms.Label lblCachedFiles;
        private System.Windows.Forms.Label lblMemoryUsageValue;
        private System.Windows.Forms.Label lblMemoryUsage;
        private System.Windows.Forms.Label lblActiveClientsValue;
        private System.Windows.Forms.Label lblActiveClients;
        private System.Windows.Forms.Button btnShowLog;
        private System.Windows.Forms.GroupBox caching;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblInterval;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnSaveSettings;
        private System.Windows.Forms.TextBox txtCacheTimeout;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}