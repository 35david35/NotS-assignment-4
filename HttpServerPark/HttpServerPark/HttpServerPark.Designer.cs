﻿namespace WindowsFormsApplication1
{
    partial class HttpServerPark
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShowLog = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.activeServers = new System.Windows.Forms.ListBox();
            this.btnToggleServers = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.serverPortStart = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.serverAmount = new System.Windows.Forms.NumericUpDown();
            this.btnSettings = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serverPortStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // btnShowLog
            // 
            this.btnShowLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnShowLog.Location = new System.Drawing.Point(271, 152);
            this.btnShowLog.Name = "btnShowLog";
            this.btnShowLog.Size = new System.Drawing.Size(132, 23);
            this.btnShowLog.TabIndex = 0;
            this.btnShowLog.Text = "Show log";
            this.btnShowLog.UseVisualStyleBackColor = true;
            this.btnShowLog.Click += new System.EventHandler(this.btnShowLog_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.activeServers);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(253, 192);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Active servers";
            // 
            // activeServers
            // 
            this.activeServers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.activeServers.FormattingEnabled = true;
            this.activeServers.Items.AddRange(new object[] {
            "Server 1: 8080",
            "Server 2: 8081",
            "Server 3: 8082",
            "Server 4: 8083"});
            this.activeServers.Location = new System.Drawing.Point(3, 16);
            this.activeServers.Name = "activeServers";
            this.activeServers.Size = new System.Drawing.Size(247, 173);
            this.activeServers.TabIndex = 0;
            this.activeServers.DoubleClick += new System.EventHandler(this.activeServers_DoubleClick);
            // 
            // btnToggleServers
            // 
            this.btnToggleServers.Location = new System.Drawing.Point(9, 103);
            this.btnToggleServers.Name = "btnToggleServers";
            this.btnToggleServers.Size = new System.Drawing.Size(117, 23);
            this.btnToggleServers.TabIndex = 1;
            this.btnToggleServers.Text = "Start";
            this.btnToggleServers.UseVisualStyleBackColor = true;
            this.btnToggleServers.Click += new System.EventHandler(this.btnToggleServers_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.serverPortStart);
            this.groupBox2.Controls.Add(this.btnToggleServers);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.serverAmount);
            this.groupBox2.Location = new System.Drawing.Point(271, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(132, 134);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sever generation";
            // 
            // serverPortStart
            // 
            this.serverPortStart.Location = new System.Drawing.Point(9, 76);
            this.serverPortStart.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.serverPortStart.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.serverPortStart.Name = "serverPortStart";
            this.serverPortStart.Size = new System.Drawing.Size(117, 20);
            this.serverPortStart.TabIndex = 5;
            this.serverPortStart.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Start on port:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Amount:";
            // 
            // serverAmount
            // 
            this.serverAmount.Location = new System.Drawing.Point(9, 37);
            this.serverAmount.Name = "serverAmount";
            this.serverAmount.Size = new System.Drawing.Size(117, 20);
            this.serverAmount.TabIndex = 2;
            // 
            // btnSettings
            // 
            this.btnSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettings.Location = new System.Drawing.Point(271, 181);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(132, 23);
            this.btnSettings.TabIndex = 3;
            this.btnSettings.Text = "Settings";
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // HttpServerPark
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 216);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnShowLog);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(429, 255);
            this.Name = "HttpServerPark";
            this.ShowIcon = false;
            this.Text = "HTTP Server Manager";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serverPortStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverAmount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox activeServers;
        private System.Windows.Forms.Button btnShowLog;
        private System.Windows.Forms.Button btnToggleServers;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown serverPortStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown serverAmount;
        private System.Windows.Forms.Button btnSettings;
    }
}

