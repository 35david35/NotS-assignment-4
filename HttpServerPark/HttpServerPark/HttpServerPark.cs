﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class HttpServerPark : Form
    {
        private static string _btnStartTextActive = "Stop";
        private static string _btnStartTextNotActive = "Start";
        private static string _httpServerPath = "notepad.exe";
        private bool _started;
        private readonly List<Process> _processes = new List<Process>();

        /// <summary>
        /// HttpServerPark Constructo
        /// </summary>
        public HttpServerPark()
        {
            _started = false;
            InitializeComponent();
        }

        /// <summary>
        /// Event handler for double click on an item in listbox
        /// Open new form to set server settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void activeServers_DoubleClick(object sender, EventArgs e)
        {
            // Get selected item from activeServers
            string serverSelected = activeServers.SelectedItem.ToString();

            if (serverSelected != null)
            {
                string server = serverSelected.Split(':')[0].Trim();
                int serverPort = Int32.Parse(serverSelected.Split(':')[1].Trim());

                //Find Server By it's port
                Object serverObject = serverPort;

                ServerForm selectedServer = new ServerForm(server, serverPort);
                selectedServer.Show();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnToggleServers_Click(object sender, EventArgs e)
        {
            if (!_started)
            {
                for (int i = 0; i < serverAmount.Value; i++)
                {
                    _processes.Add(StartNewProcess(_httpServerPath));
                }
            }
            else
            {
                foreach (Process process in _processes)
                {
                    if (!process.HasExited)
                    {
                        process.Kill();
                    }
                }
            }

            btnToggleServers.Text = _started ? _btnStartTextNotActive : _btnStartTextActive;
            _started = !_started;
        }

        /// <summary>
        /// Show log of all servers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShowLog_Click(object sender, EventArgs e)
        {
            LogForm log = new LogForm("All servers");
            log.Show();
        }

        /// <summary>
        /// Event handler for clicking Settings button
        /// Creates and opens new SettingsForm
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSettings_Click(object sender, EventArgs e)
        {
            SettingsForm settings = new SettingsForm();
            settings.Show();
        }

        /// <summary>
        /// Method to enter just numbers 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numbersOnly_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="processPath"></param>
        /// <returns></returns>
        private Process StartNewProcess(string processPath)
        {
            Process process = new Process();
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.FileName = processPath;
            process.Start();

            return process;
        }
    }
}
