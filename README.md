# NotS-assignment-4


## RESTFUL

**Include reference System.Web.Extensions when using the JSON serializer.**  
**Endpoint:** /data  
GET:  Get mockup JSON data.  
POST: Data to be sent must be valid JSON.  

If JSON parse is succesful we send {"status": "Succesfully parsed"} + JSON data.

## Session management

- Als een request nog geen cookie bevat met een uniek ID (NotsId), dan maakt de server via de response een cookie aan
- Er wordt een HttpSession object aangemaakt die deze ID bevat samen met andere waardes. Dit object wordt opgeslagen in memory op de server in een List.
- Als een request al een NotsId heeft, dan wordt er gecontroleerd of de list van HttpSession's dit NotsId bevat.
- Als de list deze bevat, wordt er niks gedaan. Anders wordt de cookie van de client verwijdert en een nieuwe aangemaakt bij een volgende request.

![FLow diagram](http://i.imgur.com/NLZMBPv.png)
