#HttpResponse Caching Headers
Beschikbare response hearders voor responses:
- Expires
- etag
- vary
- pragma
- Age
- Cache-Control (Optioneel)
- Last-Modified

---
Algehele bron: http://www.mobify.com/blog/beginners-guide-to-http-cache-headers/

Als er een default super-header voor caching zou zijn zou het Cache-Control zijn. Expires was vroeger de standaard, hiermee kan je aangeven waneer iets verlopen is, gemakkelijke met een timestamp. Toch soms nog handig voor oudere user-agents.

Cache-Control is pas beschikbaar vanaf http 1.1x

Voor browsers die gebruik maken van http 1.1 kan er dus gebruik gemaakt worden van Cache-Control. Browsers die gebruik maken van http 1.0 kan je gebruik maken van expires headers.

###Expires
Bron: https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html

The Expires entity-header field gives the date/time after which the response is considered stale. A stale cache entry may not normally be returned by a cache (either a proxy cache or a user agent cache) unless it is first validated with the origin server (or with an intermediate cache that has a fresh copy of the entity).

Conclusie:
Na de verlopen tijd mag het gecachte niet zomaar meer worden teruggegeven, er zal eerst gevalideerd moeten worden of het nog up-to-date is.

###Cache-Control
Bron: http://www.mobify.com/blog/beginners-guide-to-http-cache-headers/

If there were a default super-header for caching behaviour, this would be it.

#Gebruik Cache-Control
Voor de opdracht wordt alleen gekeken naar http 1.1, de Cache-Control header kan dus gebruikt worden.
De header wordt als volgt opgebouwd:
```
Cache-Control: private, max-age=0, no-cache
```
Cache-Control kan de volgende waardes hebben:
- no-cache: Bij elk request moet de cache opnieuw gevalideerd worden (werkt samen met bijv de etag)
- no-store: de response zal niet bewaard moeten worden
- max-age: override de expires header. Je geeft een aantal seconden mee waarna de pagina dus opnieuw opgehaald moet worden en er geen cache meer gebruikt mag worden.
- s-maxage: nvt, is voor het gebruik van CDN's
- no-transform: proxies mogen je response dan niet aanpassen.
- proxy-revalidate: voor CDN's

```
Cache-Control   = "Cache-Control" ":" 1#cache-directive

cache-directive = cache-request-directive
     | cache-response-directive

cache-request-directive =
       "no-cache"                          ; Section 14.9.1
     | "no-store"                          ; Section 14.9.2
     | "max-age" "=" delta-seconds         ; Section 14.9.3, 14.9.4
     | "max-stale" [ "=" delta-seconds ]   ; Section 14.9.3
     | "min-fresh" "=" delta-seconds       ; Section 14.9.3
     | "no-transform"                      ; Section 14.9.5
     | "only-if-cached"                    ; Section 14.9.4
     | cache-extension                     ; Section 14.9.6

 cache-response-directive =
       "public"                               ; Section 14.9.1
     | "private" [ "=" <"> 1#field-name <"> ] ; Section 14.9.1
     | "no-cache" [ "=" <"> 1#field-name <"> ]; Section 14.9.1
     | "no-store"                             ; Section 14.9.2
     | "no-transform"                         ; Section 14.9.5
     | "must-revalidate"                      ; Section 14.9.4
     | "proxy-revalidate"                     ; Section 14.9.4
     | "max-age" "=" delta-seconds            ; Section 14.9.3
     | "s-maxage" "=" delta-seconds           ; Section 14.9.3
     | cache-extension                        ; Section 14.9.6
```
Bron: https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.9

#Cache response header
Een goede response header voor deze case zou als volgt kunnen zijn:

```
Cache-Control: public, max-age=3600, no-transform
```

- public: wil zeggen dat clients / proxies de response mogen cache, private zou alleen clients het mogen Cache
- max-age=3600: de cache mag maximaal een uur bewaard worden, daarna moet die gevalideerd worden
- no-transform: Proxies mogen de content niet aanpassen
