﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    public static class Compression
    {
        public static String CompressionMethod;

        public static Byte[] DeCompressGzip(Byte[] gzip)
        {
            using (GZipStream stream = new GZipStream(new MemoryStream(gzip), CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    return memory.ToArray();
                }
            }
        }

        public static Byte[] CompressGzip(Byte[] uncompressed)
        {
            using (GZipStream stream = new GZipStream(new MemoryStream(uncompressed), CompressionMode.Compress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    return memory.ToArray();
                }
            }
        }

        public static Stream GetCompressionStream(Stream clientStream)
        {
            switch(CompressionMethod)
            {
                case "gzip":
                    return new GZipStream(clientStream,CompressionMode.Compress);
                case "deflate":
                    return new DeflateStream(clientStream,CompressionMode.Compress);
                case "chunked":
                    return new ChunkStream(clientStream);
                default:
                    return clientStream;
            }
            
        }




    }
}

