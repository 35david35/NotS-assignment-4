using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Script.Serialization;

// offered to the public domain for any use with no restriction
// and also with no warranty of any kind, please enjoy. - David Jeske.

// simple HTTP explanation
// http://www.jmarshall.com/easy/http/

namespace ConsoleApplication1
{

    public class HttpProcessor
    {
        public TcpClient Socket;
        public HttpServer Server;

        private Stream _inputStream;
        public StreamWriter OutputStream;

        public string HttpMethod;
        public string HttpUrl;
        public string HttpProtocolVersionstring;
        public Hashtable HttpHeaders = new Hashtable();

        /// <summary>
        /// Cookie Id to distinguish requests
        /// </summary>
        private const string Cookieid = "NotsId";

        private const int BUFFER_SIZE = 4096;
        private const int MAX_POST_SIZE = 10*1024*1024; // 10MB

        public HttpProcessor(TcpClient client, HttpServer server)
        {
            Socket = client;
            Server = server;
        }

        private static string StreamReadLine(Stream inputStream)
        {
            string data = "";
            while (true)
            {
                var nextChar = inputStream.ReadByte();
                if (nextChar == '\n') { break; }
                if (nextChar == '\r') { continue; }
                if (nextChar == -1) { Thread.Sleep(1); continue; }
                data += Convert.ToChar(nextChar);
            }
            return data;
        }

        public void Process()
        {
            bool authenticationRequired = false;
            // we can't use a StreamReader for input, because it buffers up extra data on us inside it'client
            // "processed" view of the world, and we want the data raw after the headers
            _inputStream = new BufferedStream(Socket.GetStream());

            // we probably shouldn't be using a streamwriter for all output from handlers either
            OutputStream = new StreamWriter(new BufferedStream(Socket.GetStream()));
            try {
                ParseRequest();
                if (HttpServer.ProtectedPaths.Contains(HttpUrl))
                    authenticationRequired = true;
                if (!ReadHeaders(authenticationRequired)) {
                    SendBasicAuthHeaders();
                } else {
                    if (HttpMethod.Equals("GET")) {
                        HandleGetRequest();
                    } else if (HttpMethod.Equals("POST")) {
                        HandlePostRequest();
                    }
                    else if (HttpMethod.Equals("PUT"))
                    {
                        HandlePutRequest();
                    }
                    else if (HttpMethod.Equals("DELETE"))
                    {
                        HandleDeleteRequest();
                    }
                }
                OutputStream.Flush();
                // bs.Flush(); // flush any remaining output
                _inputStream = null; OutputStream = null; // bs = null;
                Socket.Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception: " + exception);
                WriteFailure();
            }
        }

        public void ParseRequest()
        {
            String request = StreamReadLine(_inputStream);
            string[] tokens = request.Split(' ');
            if (tokens.Length != 3)
            {
                throw new Exception("invalid http request line");
            }
            HttpMethod = tokens[0].ToUpper();
            HttpUrl = tokens[1];
            HttpProtocolVersionstring = tokens[2];

            Console.WriteLine("starting: " + request);
        }

        /// <summary>
        /// Returns true if given string converts into set username and password
        /// </summary>
        /// <param name="credentials">value of Authorization header</param>
        /// <returns></returns>
        public bool AuthenticationOkay(string credentials) {
            string[] usernameAndPassword = Encoding.UTF8.GetString(Convert.FromBase64String(credentials)).Split(':');
            if (!HttpServer.Username.Equals(usernameAndPassword[0]) ||
                !HttpServer.Password.Equals(usernameAndPassword[1])) {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Reads headers, handles HTTP authorization
        /// </summary>
        /// <param name="authenticationRequired">If true there'll be checked for valid authentication credentials</param>
        /// <returns>True if request may be served, false otherwise</returns>
        public bool ReadHeaders(bool authenticationRequired) {
            bool mayServeResponse = true;
            bool includesAuthHeaders = false;
            Console.WriteLine("readHeaders()");
            String line;
            while ((line = StreamReadLine(_inputStream)) != null)
            {
                if (line.Equals(""))
                {
                    Console.WriteLine("got headers");
                    if (!includesAuthHeaders && authenticationRequired) {
                        return false;
                    }
                    return mayServeResponse;
                }

                int separator = line.IndexOf(':');
                if (separator == -1)
                {
                    throw new Exception("invalid http header line: " + line);
                }
                string name = line.Substring(0, separator);
                int position = separator + 1;
                while ((position < line.Length) && (line[position] == ' '))
                {
                    position++; // strip any spaces
                }

                string value = line.Substring(position, line.Length - position);

                //Handle authorization
                if ("Authorization".Equals(name)) {
                    includesAuthHeaders = true;
                    if (HttpServer.HttpAuthOn && authenticationRequired) {
                        mayServeResponse = AuthenticationOkay(value.Split(' ')[1]);
                    }
                }

                Console.WriteLine("header: {0}:{1}",name,value);
                HttpHeaders[name] = value;
            }
            if (!includesAuthHeaders) {
                return false;
            }
            return mayServeResponse;
        }

        private void HandleDeleteRequest()
        {
            Console.WriteLine("get post data start");
            int content_len = 0;
            MemoryStream ms = new MemoryStream();
            if (this.HttpHeaders.ContainsKey("Content-Length"))
            {
                content_len = Convert.ToInt32(this.HttpHeaders["Content-Length"]);
                if (content_len > MAX_POST_SIZE)
                {
                    throw new Exception(
                        String.Format("PUT Content-Length({0}) too big for this simple server",
                          content_len));
                }
                byte[] buf = new byte[BUFFER_SIZE];
                int to_read = content_len;
                while (to_read > 0)
                {
                    Console.WriteLine("starting Read, to_read={0}", to_read);

                    int numread = this._inputStream.Read(buf, 0, Math.Min(BUFFER_SIZE, to_read));
                    Console.WriteLine("read finished, numread={0}", numread);
                    if (numread == 0)
                    {
                        if (to_read == 0)
                        {
                            break;
                        }
                        else {
                            throw new Exception("client disconnected during post");
                        }
                    }
                    to_read -= numread;
                    ms.Write(buf, 0, numread);
                }
                ms.Seek(0, SeekOrigin.Begin);
            }
            Console.WriteLine("get post data end");
            Server.HandleDeleteRequest(this, new StreamReader(ms));
        }

        private void HandlePutRequest()
        {
            Console.WriteLine("get post data start");
            int content_len = 0;
            MemoryStream ms = new MemoryStream();
            if (this.HttpHeaders.ContainsKey("Content-Length"))
            {
                content_len = Convert.ToInt32(this.HttpHeaders["Content-Length"]);
                if (content_len > MAX_POST_SIZE)
                {
                    throw new Exception(
                        String.Format("DELETE Content-Length({0}) too big for this simple server",
                          content_len));
                }
                byte[] buf = new byte[BUFFER_SIZE];
                int to_read = content_len;
                while (to_read > 0)
                {
                    Console.WriteLine("starting Read, to_read={0}", to_read);

                    int numread = this._inputStream.Read(buf, 0, Math.Min(BUFFER_SIZE, to_read));
                    Console.WriteLine("read finished, numread={0}", numread);
                    if (numread == 0)
                    {
                        if (to_read == 0)
                        {
                            break;
                        }
                        else {
                            throw new Exception("client disconnected during post");
                        }
                    }
                    to_read -= numread;
                    ms.Write(buf, 0, numread);
                }
                ms.Seek(0, SeekOrigin.Begin);
            }
            Console.WriteLine("get post data end");
            Server.HandlePutRequest(this, new StreamReader(ms));
        }

        public void HandleGetRequest()
        {
            Server.HandleGetRequest(this);
        }

        public void SendBasicAuthHeaders() {
            Server.SendBasicAuthHeaders(this);
        }

        public void HandlePostRequest() {
            // this post data processing just reads everything into a memory stream.
            // this is fine for smallish things, but for large stuff we should really
            // hand an input stream to the request processor. However, the input stream
            // we hand him needs to let him see the "end of the stream" at this content
            // length, because otherwise he won't know when he'client seen it all!

            Console.WriteLine("get post data start");
            MemoryStream memoryStream = new MemoryStream();
            if (HttpHeaders.ContainsKey("Content-Length"))
            {
                var contentLength = Convert.ToInt32(HttpHeaders["Content-Length"]);
                if (contentLength > MAX_POST_SIZE)
                {
                    throw new Exception(
                        $"POST Content-Length({contentLength}) too big for this simple server");
                }

                byte[] buffer = new byte[BUFFER_SIZE];
                int toRead = contentLength;
                while (toRead > 0)
                {
                    Console.WriteLine("starting Read, to_read={0}", toRead);

                    int numread = _inputStream.Read(buffer, 0, Math.Min(BUFFER_SIZE, toRead));
                    Console.WriteLine("read finished, numread={0}", numread);
                    if (numread == 0)
                    {
                        if (toRead == 0)
                        {
                            break;
                        }
                        throw new Exception("client disconnected during post");
                    }
                    toRead -= numread;
                    memoryStream.Write(buffer, 0, numread);
                }
                memoryStream.Seek(0, SeekOrigin.Begin);
            }
            Console.WriteLine("get post data end");
            Server.HandlePostRequest(this, new StreamReader(memoryStream));
        }

        public void WriteSuccess(string cookie, string contentType = "text/html")
        {
            // this is the successful HTTP response line
            OutputStream.WriteLine("HTTP/1.0 200 OK");
            // these are the HTTP headers...
            OutputStream.WriteLine("Content-Type: " + contentType);
            OutputStream.WriteLine("Connection: close");
            // ..add your own headers here if you like


            //Check if cookie has notsId, if not add notsid to response as a cookie
            if (!cookie.Contains(Cookieid)) {
                HttpSession httpSession = new HttpSession(Guid.NewGuid()) { Name = "kaas" };
                OutputStream.WriteLine("Set-Cookie: " + Cookieid + "=" + httpSession.Guid);
                if (HttpServer.HttpSessions.Find(sessie => sessie.Guid == httpSession.Guid) == null)
                    HttpServer.HttpSessions.Add(httpSession);
            } else { //If cookie notsId already exists, check if it's in the httpSession list, if not remove cookie
                Guid? guid = null;
                string[] cookieKeyValStrings = Regex.Split(cookie, ";");
                foreach (var cookieKeyVal in cookieKeyValStrings) {
                    if (cookieKeyVal.Contains(Cookieid)) {
                        guid = Guid.Parse(cookieKeyVal.Replace(Cookieid + "=", ""));
                    }
                }

                //if the guid is parsed correctly, check the httpsessionslist if it's not found, remove the cookie from the client
                if (guid != null) {
                    if (HttpServer.HttpSessions.Find(sessie => sessie.Guid == guid) == null) {
                        OutputStream.WriteLine("Set-Cookie: " + Cookieid + "=deleted; expires=Thu, 01 Jan 1970 00:00:00 GMT");
                    }
                }
            }


            OutputStream.WriteLine(""); // this terminates the HTTP headers.. everything after this is HTTP body..
        }

        public void WriteFailure()
        {
            // this is an http 404 failure response
            OutputStream.WriteLine("HTTP/1.0 404 File not found");
            // these are the HTTP headers
            OutputStream.WriteLine("Connection: close");
            // ..add your own headers here

            OutputStream.WriteLine(""); // this terminates the HTTP headers.
        }
    }

    public class HttpSession {
        public Guid Guid { get; private set; }
        public string Name { get; set; }

        public HttpSession(Guid guid) {
            Guid = guid;
        }
    }


    public abstract class HttpServer
    {
        protected int Port;
        private TcpListener _listener;
        private bool _isActive = true;
        public static List<HttpSession> HttpSessions = new List<HttpSession>();
        internal static bool HttpAuthOn = true;
        internal static string Username = "nots";
        internal static string Password = "nots";
        internal static string[] ProtectedPaths = { "/admin" };

        protected int ConnectionId;
        protected ListenPipe _listenPipe;
        protected SendPipe _sendPipe;
        private readonly int _pipeTimeout = 1000;

        protected HttpServer(int port, int connectionId) {
            Port = port;
            ConnectionId = connectionId;


            _listenPipe = new ListenPipe("GuiPipe" + ConnectionId);
            _listenPipe.messageDelegate += HandlePipeCommand;
            _listenPipe.Listen();

            _sendPipe = new SendPipe("ConsolePipe" + ConnectionId);
        }

        private void HandlePipeCommand(string request) {
            string result = request.Replace("\0", String.Empty);
            switch (result) {
                case "1": //Port
                    _sendPipe.Send(Port.ToString(), _pipeTimeout);
                    break;
                case "2": //Load
                    PerformanceCounter cpuCounter = new PerformanceCounter();
                    cpuCounter.CategoryName = "Processor";
                    cpuCounter.CounterName = "% Processor Time";
                    cpuCounter.InstanceName = "_Total";
                    _sendPipe.Send(cpuCounter.NextValue() + "%", _pipeTimeout);
                    break;
                case "3": //Memory
                    _sendPipe.Send(Math.Round(GC.GetTotalMemory(false) / 1024.0 / 1024.0, 2) + " MB", _pipeTimeout);
                    break;
                case "5": //nThreads
                    _sendPipe.Send(Process.GetCurrentProcess().Threads.Count.ToString(), _pipeTimeout);
                    break;
            }
        }

        public void Listen() {
            _listener = new TcpListener(IPAddress.Any, Port);
            _listener.Start();
            while (_isActive) {
                TcpClient client = _listener.AcceptTcpClient();
                HttpProcessor processor = new HttpProcessor(client, this);
                Thread thread = new Thread(processor.Process);
                thread.Start();
                Thread.Sleep(1);
            }
        }

        public abstract void HandleGetRequest(HttpProcessor p);
        public abstract void HandlePostRequest(HttpProcessor p, StreamReader inputData);
        public abstract void HandlePutRequest(HttpProcessor httpProcessor, StreamReader streamReader);

        public abstract void HandleDeleteRequest(HttpProcessor httpProcessor, StreamReader streamReader);

        public abstract void SendBasicAuthHeaders(HttpProcessor p);
    }

    public class MyHttpServer : HttpServer
    {
        public MyHttpServer(int port, int connectionId) : base(port, connectionId)
        {
        }

        private string GetCookie(HttpProcessor p) {
            string cookies = "";
            if (p.HttpHeaders.ContainsKey("Cookie")) {
                cookies = p.HttpHeaders["Cookie"].ToString();
            }
            return cookies;
        }


        public override void HandleGetRequest(HttpProcessor processor)
        {
            if (processor.HttpUrl.Equals("/data"))
            {
                processor.WriteSuccess(GetCookie(processor),"application/json");

                var mockData = new MockJSONData {courseName = "NotS", teacher = "Theo Theunissen", nrOfStudents = 25};
                var serializedData = new JavaScriptSerializer().Serialize(mockData);
                processor.OutputStream.WriteLine(serializedData);
            }
            else
            {
                var requestFile = processor.HttpUrl == "/" ? "index.html" : processor.HttpUrl;
                var file = System.IO.Directory.GetCurrentDirectory() + "\\..\\..\\..\\TestWebsite\\" + requestFile;
                if (file.EndsWith("/"))
                {
                    file += "index.html";
                }

                if (!File.Exists(file))
                {
                    processor.WriteFailure();
                }
                else
                {
                    var bytes = File.ReadAllBytes(file);
                    var contentType = (string)processor.HttpHeaders["Content-Type"];
                    processor.WriteSuccess(GetCookie(processor), contentType);
                    processor.OutputStream.BaseStream.Write(bytes, 0, bytes.Length);
                }
            }
        }

        public override void HandlePostRequest(HttpProcessor processor, StreamReader inputData)
        {
            Console.WriteLine("POST request: {0}", processor.HttpUrl);
            string data = inputData.ReadToEnd();

            processor.WriteSuccess(GetCookie(processor));
            processor.OutputStream.WriteLine("<html><body><h1>test server</h1>");
            processor.OutputStream.WriteLine("<a href=/test>return</a><processor>");
            processor.OutputStream.WriteLine("postbody: <pre>{0}</pre>", data);
        }

        public override void SendBasicAuthHeaders(HttpProcessor p) {
            Console.WriteLine("Sending basic auth headers for " + p.HttpUrl);
            p.OutputStream.WriteLine("HTTP/1.1 401 Unauthorized");
            p.OutputStream.WriteLine("WWW-Authenticate: Basic realm=\"NotS\"");
            p.OutputStream.WriteLine("Connection: close");
            p.OutputStream.WriteLine("");
        }

        public override void HandlePutRequest(HttpProcessor p, StreamReader inputData)
        {
            Console.WriteLine("PUT request: {0}", p.HttpUrl);
            string data = inputData.ReadToEnd();

            if (p.HttpUrl.Equals("/data"))
            {
                p.WriteSuccess(GetCookie(p), "application/json");

                var serializer = new JavaScriptSerializer();
                dynamic deserializedData = serializer.Deserialize<ExpandoObject>(data);

                deserializedData.status = "Succesfully parsed PUT data";
                deserializedData.data = data;
                var serializedJsonData = serializer.Serialize(deserializedData);

                p.OutputStream.WriteLine(serializedJsonData);

            }
            else {
                p.WriteSuccess(GetCookie(p));
                p.OutputStream.WriteLine("<html><body><h1>test server</h1>");
                p.OutputStream.WriteLine("<a href=/test>return</a><p>");
                p.OutputStream.WriteLine("putbody: <pre>{0}</pre>", data);
            }
        }

        public override void HandleDeleteRequest(HttpProcessor p, StreamReader inputData)
        {
            Console.WriteLine("DELETE request: {0}", p.HttpUrl);
            string data = inputData.ReadToEnd();

            if (p.HttpUrl.Equals("/data"))
            {
                p.WriteSuccess(GetCookie(p), String.Intern("application/json"));

                var serializer = new JavaScriptSerializer();
                dynamic deserializedData = serializer.Deserialize<ExpandoObject>(data);

                deserializedData.status = "Succesfully parsed DELETE data";
                deserializedData.data = data;
                var serializedJsonData = serializer.Serialize(deserializedData);

                p.OutputStream.WriteLine(serializedJsonData);

            }
            else {
                p.WriteSuccess(GetCookie(p));
                p.OutputStream.WriteLine("<html><body><h1>test server</h1>");
                p.OutputStream.WriteLine("<a href=/test>return</a><p>");
                p.OutputStream.WriteLine("putbody: <pre>{0}</pre>", data);
            }
        }
    }

    public class TestMain
    {
        public static int Main(string[] args)
        {
            int connectionId = args.Length > 0 ? int.Parse(args[0]) : 0;
            int port = args.Length > 1 ? int.Parse(args[1]) : 8080;

            HttpServer httpServer = new MyHttpServer(port, connectionId);
            Thread thread = new Thread(httpServer.Listen);
            thread.Start();
            return 0;
        }
    }
}
