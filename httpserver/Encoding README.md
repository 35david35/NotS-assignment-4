# Content Encoding
De content encoding header in een bericht bepaald hoe de data compressed wordt voor deze verstuurd moet worden.

# Gzip en Deflate
De meest gebruikte vormen van compressie via HTTP zijn gzip en Deflate. HTTP compressie kan in web servers en web clients worden verwerkt om snelheid te kunnen verbeteren en data grootte te verkleinen. HTTP data wordt compressed voor het verstuurd wordt vanaf de server.

# Transfer Encoding
Wanneer een Transfer-Encoding header in het bericht zit wordt de standaard HTTP identitiy encoding vervangen.

Wanneer een HTTP bericht een content-length bevat zal hier de grootte van de body in staan. Wanneer er een transfer-encoding header is zal de content-length header niet bestaan of de content-length wordt genegeerd. De content-length is dan nutteloos omdat de transfer encoding de werking van hoe de body wordt verstuurd veranderd. (en waarschijnlijk de lengte van de bytes die verstuurd moeten worden)

# Chunked Transfer Encoding
Chunked transfer encoding is een data verstuur mechanisme in versie 1.1 van HTTP. De data wordt verstuurd in chunks. De Transfer Encoding HTTP header neemt de plaats in van de Content-Length header. Omdat de Content-Length header niet gebruikt wordt weet de verstuurder niet de grootte van de content voor deze verstuurd wordt naar de ontvanger.

De grootte van iedere chunk wordt eerst verstuurd zodat de ontvanger weet wanneer alles ontvangen is voor deze chunk. De transfer wordt gestopt wanneer de laatste chunk met de lengte van nul verstuurd is.
