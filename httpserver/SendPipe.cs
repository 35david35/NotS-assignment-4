﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class SendPipe
    {
        private string CONNECTIONID = "ConsolePipe";
        public SendPipe(string ConnectionID)
        {
            this.CONNECTIONID = ConnectionID;
        }


        public void Send(string message, int timeout)
        {
            try{
                NamedPipeClientStream pipestream = new NamedPipeClientStream(".", CONNECTIONID, PipeDirection.Out, PipeOptions.Asynchronous);
                // The connect function will indefinitely wait for the pipe to become available
                // If that is not acceptable specify a maximum waiting time (in ms)
                pipestream.Connect(timeout);
                byte[] _buffer = Encoding.UTF8.GetBytes(message);
                pipestream.BeginWrite(_buffer, 0, _buffer.Length, AsyncSend, pipestream);


            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void AsyncSend(IAsyncResult iar)
        {
            try
            {
                // Get the pipe
                NamedPipeClientStream pipeStream = (NamedPipeClientStream)iar.AsyncState;

                // End the write
                pipeStream.EndWrite(iar);
                pipeStream.Flush();
                pipeStream.Close();
                pipeStream.Dispose();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
